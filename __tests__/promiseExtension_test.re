open Jest;
open Helpers;
open Js_promise;

open PromiseExtension.PromiseMonad; 

let f s => s ^ "!";

let base = resolve "hello";

let result = resolve "hello!";

let _ =

describe "PromiseExtension" (fun () => {
  open Expect;

  test "fmap" (fun () =>
    expect (fmap f base) |> toEqual result);

  test "seq" (fun () =>
    expect (seq (pure f) base) |> toEqual result);

  test ">>=" (fun () =>
    expect (base >>= (return % f)) |> toEqual result);
});
