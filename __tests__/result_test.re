open Helpers;
open Jest;

module MyErrors = {
  type t = ArgumentError | DivisionByZeroError;
};

module MyResult = Result.Functor(MyErrors);

open MyResult;
open MyResult.ResultMonad;

let f _ => "error";
let g s => s ^ "!";
let def = Option.default "nothing";

let ok = Ok "no error";
let err = Err MyErrors.ArgumentError;
let result = Ok "no error!";

let _ =

describe "Result" (fun () => {
  open Expect;

  test "rescue ok" (fun () =>
    expect (ok >! f) |> toBe "no error");

  test "rescue err" (fun () =>
    expect (err >! f) |> toBe "error");

  test "toOption ok" (fun () =>
    expect (ok |> toOption |> def) |> toBe "no error");

  test "toOption err" (fun () =>
    expect (err |> toOption |> def) |> toBe "nothing");

  test "orElse" (fun () =>
    expect (err <|> ok) |> toEqual (Some ok));

  test "orElse" (fun () =>
    expect (ok <|> err) |> toEqual (Some ok));

  test "orElse" (fun () =>
    expect (err <|> err) |> toEqual None);

  test "fmap" (fun () =>
    expect (fmap g ok) |> toEqual result);

  test "fmap err" (fun () =>
    expect (fmap g err) |> toEqual err);

  test "<$" (fun () =>
    expect ("no error!" <$ ok) |> toEqual result);

  test "seq" (fun () =>
    expect (seq (pure g) ok) |> toEqual result);

  test "seq err" (fun () =>
    expect (seq (pure g) err) |> toEqual err);

  test ">>=" (fun () =>
    expect (ok >>= (return % g)) |> toEqual result);

  test ">>= err" (fun () =>
    expect (err >>= (return % g)) |> toEqual err);

});
