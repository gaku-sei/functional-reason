open Jest;
open Helpers;

open OptionExtension.OptionMonad;

let f s => s ^ "!";

let base = Some "hello";

let result = Some "hello!";

let _ =

describe "OptionExtension" (fun () => {
  open Expect;

  test "fmap" (fun () =>
    expect (fmap f base) |> toEqual result);

  test "fmap none" (fun () =>
    expect (fmap f None) |> toEqual None);

  test "<$" (fun () =>
    expect ("hello!" <$ base) |> toEqual result);

  test "seq" (fun () =>
    expect (seq (pure f) base) |> toEqual result);

  test "seq none" (fun () =>
    expect (seq (pure f) None) |> toEqual None);

  test ">>=" (fun () =>
    expect (base >>= (return % f)) |> toEqual result);

  test ">>= none" (fun () =>
    expect (None >>= (return % f)) |> toEqual None);
});
