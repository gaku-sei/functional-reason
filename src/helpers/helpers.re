/*Waiting for BatPervasives for Reason*/
let const a => fun _ => a;

let comp f g x => f (g x);

let (%) = comp;

let flip f x y => f y x;
