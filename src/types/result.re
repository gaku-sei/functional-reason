open Applicative;
open Functor;
open Monad;
open Typed;

module Functor (Module: Typed) => {
  type errors = Module.t;

  type result 'a 'b = Ok 'a | Err 'b constraint 'b = errors;

  let toOption = fun
    | Ok x => Some x
    | _ => None;

  let rescue (res: result 'a 'b) (f: 'b => 'a) => switch res {
    | Ok x => x;
    | Err y => f y;
  };

  let (>!) = rescue;

  let orElse res1 res2 => switch (res1, res2) {
    | (Ok _, _) => Some res1;
    | (_, Ok _) => Some res2;
    | _ => None;
  };

  let (<|>) = orElse;

  module ResultFunctorImpl: FunctorSig with type f 'a = result 'a errors = {
    type f 'a = result 'a errors;

    let fmap f r => switch r {
    | Ok a => Ok (f a);
    | Err _ as e => e;
    };
  };

  module ResultFunctor = Functor(ResultFunctorImpl);

  module ResultApplicativeImpl: ApplicativeSig with type f 'a = result 'a errors = {
    include ResultFunctor;

    let pure a => Ok a;

    let seq f r => switch f {
    | Ok f => fmap f r;
    | Err _ as e => e;
    };
  };

  module ResultApplicative = Applicative(ResultApplicativeImpl);

  module ResultMonadImpl: MonadSig with type f 'a = result 'a errors = {
    include ResultApplicative;

    let (>>=) r f => switch r {
    | Ok a => f a;
    | Err _ as e => e;
    };
  };

  module ResultMonad = Monad(ResultMonadImpl);
};
