open Helpers;

module type FunctorSig = {
  type f _;

  let fmap: ('a => 'b) => f 'a => f 'b;
};

module type FullFunctorSig = {
  include FunctorSig;

  let (<$): 'a => f 'b => f 'a;
};

module Functor (Module: FunctorSig) => {
  include Module;

  /*function composition does not seem to work here*/
   let (<$) a => fmap (const a);
};
