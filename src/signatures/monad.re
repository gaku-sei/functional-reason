open Applicative;

module type MonadSig = {
  include FullApplicativeSig;

  let (>>=): f 'a => ('a => f 'b) => f 'b;
};

module type FullMonadSig = {
  include MonadSig;

  let (>>): f 'a => f 'b => f 'b;

  let return: 'a => f 'a;
};

module Monad (Module: MonadSig) => {
  include Module;

  let (>>) = (*>);

  let return = pure;
};
