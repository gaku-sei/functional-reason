open Functor;

module type ApplicativeSig = {
  include FullFunctorSig;

  let pure: 'a => f 'a;

  let seq: f ('a => 'b) => f 'a => f 'b;
};

module type FullApplicativeSig = {
  include ApplicativeSig;

  let (*>): f 'a => f 'b => f 'b;
};

module Applicative (Module: ApplicativeSig) => {
  include Module;

  let (*>) _ b => b;
};
