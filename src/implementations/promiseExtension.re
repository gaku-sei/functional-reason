open Js_promise;
open Helpers;

open Applicative;
open Functor;
open Monad;

module PromiseFunctorImpl: FunctorSig with type f 'a = t 'a = {
  type f 'a = t 'a;

  let fmap f p => then_ (resolve % f) p;
};

module PromiseFunctor = Functor(PromiseFunctorImpl);

module PromiseApplicativeImpl: ApplicativeSig with type f 'a = t 'a = {
  include PromiseFunctor;

  let pure = resolve;

  let seq f p => then_ ((flip fmap) p) f;
};

module PromiseApplicative = Applicative(PromiseApplicativeImpl);

module PromiseMonadSig: MonadSig with type f 'a = t 'a = {
  include PromiseApplicative;

  /*for any reason flip does not work here*/
  let (>>=) p f => then_ f p;
};

module PromiseMonad = Monad(PromiseMonadSig);
