open Applicative;
open Functor;
open Monad;

module OptionFunctorImpl: FunctorSig with type f 'a = option 'a = {
  type f 'a = option 'a;

  let fmap f o => switch o {
  | Some a => Some (f a);
  | _ => None;
  };
};

module OptionFunctor = Functor(OptionFunctorImpl);

module OptionApplicativeImpl: ApplicativeSig with type f 'a = option 'a = {
  include OptionFunctor;

  let pure a => Some a;

  let seq f o => switch f {
  | Some f => fmap f o;
  | _ => None;
  };
};

module OptionApplicative = Applicative(OptionApplicativeImpl);

module OptionMonadImpl: MonadSig with type f 'a = option 'a = {
  include OptionApplicative;

  let (>>=) o f => switch o {
  | Some a => f a;
  | _ => None;
  };
};

module OptionMonad = Monad(OptionMonadImpl);
